import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_achivement_list/router/router_core.dart';

import 'app.dart';
import 'cubit/app_state_cubit.dart';

extension AppEx on App {
  static ThemeData get theme => Theme.of(RouterCore.navigatorKey.currentContext!);
  static AppStateCubit get bloc => BlocProvider.of<AppStateCubit>(RouterCore.navigatorKey.currentContext!);
  static AppStateState get state => BlocProvider.of<AppStateCubit>(RouterCore.navigatorKey.currentContext!).state;
}

extension ThemeDataEx on ThemeData {
  Color color({
    required Color light,
    required Color dark,
  }) => this.brightness == Brightness.light ? light : dark;
}
