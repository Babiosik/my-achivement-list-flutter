part of 'app_state_cubit.dart';

class AppStateState {
  final ThemeMode themeMode;
  AppStateState({
    this.themeMode = ThemeMode.light,
  });

  AppStateState copyWith({
    ThemeMode? themeMode,
  }) {
    return AppStateState(
      themeMode: themeMode ?? this.themeMode,
    );
  }
}
