import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_achivement_list/helpers/cache.dart';

part 'app_state_state.dart';

class AppStateCubit extends Cubit<AppStateState> {
  AppStateCubit() : super(AppStateState());

  void setBrightness(ThemeMode themeMode) {
    Cache.darkMode = themeMode == ThemeMode.dark;
    emit(state.copyWith(themeMode: themeMode));
  }

  static AppStateCubit find(BuildContext context) {
    return BlocProvider.of<AppStateCubit>(context);
  }
}
