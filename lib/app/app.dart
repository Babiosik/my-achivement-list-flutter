import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_achivement_list/config/app_themes.dart';
import 'package:my_achivement_list/router/router_core.dart';
import 'package:my_achivement_list/router/router_list.dart';

import 'cubit/app_state_cubit.dart';

class App extends StatefulWidget {
  final AppStateCubit bloc;
  App._(this.bloc);

  static Widget build() {
    print("View: AppView -- build");
    return BlocProvider(
      create: (context) => AppStateCubit(),
      child: BlocBuilder<AppStateCubit, AppStateState>(
        builder: (BuildContext context, AppStateState state) {
          return App._(BlocProvider.of<AppStateCubit>(context));
        },
      ),
    );
  }

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      themeMode: widget.bloc.state.themeMode,
      theme: AppThemes.light,
      darkTheme: AppThemes.dark,
      initialRoute: R.LoadingRoute,
      onGenerateRoute: RouterCore.generate,
      navigatorKey: RouterCore.navigatorKey,
    );
  }
}
