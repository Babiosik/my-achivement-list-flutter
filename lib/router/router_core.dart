import 'package:flutter/material.dart';

import 'router_list.dart';

abstract class RouterCore {
  RouterCore._();

  static final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();

  static late String _defaultRoute;

  static String get defaultRoute => _defaultRoute;

  static Future<void> init({required String defaultRoute}) async {
    _defaultRoute = defaultRoute;
  }

  static Route<dynamic> generate(RouteSettings settings) {
    return builder[settings.name] != null
        ? builder[settings.name]!(settings.arguments)
        : builder[_defaultRoute]!(settings.arguments);
  }

  static PageRouteBuilder route(Widget child) {
    return PageRouteBuilder(
      opaque: false,
      pageBuilder: (context, _, __) => child,
    );
  }

  static Future<T?> goTo<T extends Object?>(
    String view, {
    bool replacement = false,
    Object? arguments,
    BuildContext? context,
  }) async {
    if (replacement) {
      if (context != null) {
        return Navigator.of(context).pushReplacementNamed(view, arguments: arguments);
      } else {
        return navigatorKey.currentState!.pushReplacementNamed(view, arguments: arguments);
      }
    } else {
      if (context != null) {
        return Navigator.of(context).pushNamed(view, arguments: arguments);
      } else {
        return navigatorKey.currentState!.pushNamed(view, arguments: arguments);
      }
    }
  }

  static Future<void> pop<T extends Object>({Object? data, BuildContext? context}) async {
    if (context != null) {
      return Navigator.of(context).pop(data);
    } else {
      return navigatorKey.currentState!.pop(data);
    }
  }
}
