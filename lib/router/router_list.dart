import 'package:flutter/material.dart';
import 'package:my_achivement_list/features/color_group_create/color_group_create_view.dart';
import 'package:my_achivement_list/features/color_group_settings/color_group_settings_view.dart';
import 'package:my_achivement_list/features/loading/loading_view.dart';
import 'package:my_achivement_list/features/main/main_view.dart';
import 'package:my_achivement_list/features/settings/settings_view.dart';
import 'package:my_achivement_list/features/task/task/task_view.dart';
import 'package:my_achivement_list/features/task/task_create/task_create_view.dart';

// @TODO Сюда собирать роуты
abstract class R {
  static const String LoadingRoute = "Loading";
  static const String MainRoute = "Main";
  static const String TaskCreateRoute = "TaskCreate";
  static const String TaskRoute = "Task";
  static const String SettingsRoute = "Settings";
  static const String ColorGroupSettingsRoute = "ColorGroupSettings";
  static const String ColorGroupCreateRoute = "ColorGroupCreate";
}

final Map<String, PageRoute Function(Object?)?> builder = {
  R.LoadingRoute: (Object? arguments) => LoadingView.route(),
  R.MainRoute: (Object? arguments) => MainView.route(),
  R.TaskCreateRoute: (Object? arguments) => TaskCreateView.route(),
  R.TaskRoute: (Object? arguments) => TaskView.route(arguments),
  R.SettingsRoute: (Object? arguments) => SettingsView.route(),
  R.ColorGroupSettingsRoute: (Object? arguments) => ColorGroupSettingsView.route(),
  R.ColorGroupCreateRoute: (Object? arguments) => ColorGroupCreateView.route(),
};
