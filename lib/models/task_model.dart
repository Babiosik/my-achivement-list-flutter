import 'dart:convert';

import 'package:my_achivement_list/models/color_group_model.dart';

class TaskModel {
  final String text;
  final DateTime dateTime;
  final ColorGroupModel colorGroup;

  TaskModel({
    required this.text,
    required this.dateTime,
    required this.colorGroup,
  });

  TaskModel copyWith({
    String? text,
    DateTime? dateTime,
    ColorGroupModel? colorGroup,
  }) {
    return TaskModel(
      text: text ?? this.text,
      dateTime: dateTime ?? this.dateTime,
      colorGroup: colorGroup ?? this.colorGroup,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'text': text,
      'dateTime': dateTime.millisecondsSinceEpoch,
      'colorGroup': colorGroup.toMap(),
    };
  }

  factory TaskModel.fromMap(Map<String, dynamic> map) {
    return TaskModel(
      text: map['text'],
      dateTime: DateTime.fromMillisecondsSinceEpoch(map['dateTime']),
      colorGroup: map['colorGroup'] != null
          ? ColorGroupModel.fromMap(map['colorGroup'])
          : ColorGroupModel.zero,
    );
  }

  String toJson() => json.encode(toMap());

  factory TaskModel.fromJson(String source) => TaskModel.fromMap(json.decode(source));

  @override
  String toString() => 'TaskModel(text: $text, dateTime: $dateTime, colorGroup: $colorGroup)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is TaskModel &&
        other.text == text &&
        other.dateTime == dateTime &&
        other.colorGroup == colorGroup;
  }

  @override
  int get hashCode => text.hashCode ^ dateTime.hashCode ^ colorGroup.hashCode;
}
