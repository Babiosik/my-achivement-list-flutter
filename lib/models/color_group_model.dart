import 'dart:convert';

import 'package:flutter/material.dart';

class ColorGroupModel {
  final int colorHex;
  final String name;

  ColorGroupModel({
    required this.colorHex,
    required this.name,
  });

  Color get color => Color(colorHex);

  static ColorGroupModel get zero => ColorGroupModel(colorHex: 0x00000000, name: 'Unselected group');

  ColorGroupModel copyWith({
    int? colorHex,
    String? name,
  }) {
    return ColorGroupModel(
      colorHex: colorHex ?? this.colorHex,
      name: name ?? this.name,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'colorHex': colorHex,
      'name': name,
    };
  }

  factory ColorGroupModel.fromMap(Map<String, dynamic> map) {
    return ColorGroupModel(
      colorHex: map['colorHex'],
      name: map['name'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ColorGroupModel.fromJson(String source) => ColorGroupModel.fromMap(json.decode(source));

  @override
  String toString() => 'ColorGroupModel(colorHex: $colorHex, name: $name)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
  
    return other is ColorGroupModel &&
      other.colorHex == colorHex &&
      other.name == name;
  }

  @override
  int get hashCode => colorHex.hashCode ^ name.hashCode;
}
