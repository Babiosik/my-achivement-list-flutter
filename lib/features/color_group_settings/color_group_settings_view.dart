import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_achivement_list/app/app_ex.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';
import 'package:my_achivement_list/models/color_group_model.dart';
import 'package:my_achivement_list/widgets/app_back_button.dart';
import 'package:my_achivement_list/widgets/dismissible_screen.dart';

import 'cubit/color_group_settings_cubit.dart';

class ColorGroupSettingsView extends StatefulWidget {
  final ColorGroupSettingsCubit bloc;
  ColorGroupSettingsView._(this.bloc);

  static Widget build() {
    print("View: ColorGroupSettingsView -- build");
    return BlocProvider(
      create: (context) => ColorGroupSettingsCubit()..fetch(),
      child: BlocBuilder<ColorGroupSettingsCubit, ColorGroupSettingsState>(
        builder: (BuildContext context, ColorGroupSettingsState state) {
          return ColorGroupSettingsView._(BlocProvider.of<ColorGroupSettingsCubit>(context));
        },
      ),
    );
  }

  static PageRouteBuilder route() => DismissibleScreen.route(() => ColorGroupSettingsView.build());

  @override
  _ColorGroupSettingsViewState createState() => _ColorGroupSettingsViewState();
}

class _ColorGroupSettingsViewState extends State<ColorGroupSettingsView> {
  final _listKey = GlobalKey<AnimatedListState>();

  @override
  Widget build(BuildContext context) {
    return DismissibleScreen(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Color groups'),
          leading: AppBackButton(),
        ),
        body: widget.bloc.state.list.length > 0
            ? AnimatedList(
                key: _listKey,
                initialItemCount: widget.bloc.state.list.length,
                itemBuilder: (BuildContext context, int index, animation) => SlideTransition(
                  position: animation.drive(Tween(
                    begin: Offset(1, 0),
                    end: Offset(0, 0),
                  )),
                  child: _colorItem(index, widget.bloc.state.list[index]),
                ),
              )
            : Container(),
        floatingActionButton: FloatingActionButton(
          onPressed: () async {
            int index = await widget.bloc.addColorGroup();
            await Future.delayed(Duration(milliseconds: 400));
            _listKey.currentState?.insertItem(index);
          },
          child: Icon(Icons.add),
        ),
      ),
    );
  }

  Widget _colorItem(int index, ColorGroupModel colorGroup) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.endToStart,
      onDismissed: (_) {
        Duration _duration = Duration.zero;
        _listKey.currentState?.removeItem(
          index,
          (context, animation) => Container(),
          duration: _duration,
        );
        widget.bloc.removeColorGroup(index);
      },
      background: Container(
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: AppSpaces.sm),
        child: Icon(Icons.delete, color: Colors.red),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: AppEx.theme.canvasColor,
          border: Border.all(
            color: AppEx.theme.color(
              light: Colors.black26,
              dark: Colors.white12,
            ),
          ),
        ),
        padding: EdgeInsets.all(AppSpaces.def),
        child: Row(
          children: [
            Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: colorGroup.color,
              ),
            ),
            AppSpaces.spaceW(AppSpaces.sm),
            Expanded(
              child: Text(colorGroup.name),
            ),
          ],
        ),
      ),
    );
  }
}
