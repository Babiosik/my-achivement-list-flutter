import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:my_achivement_list/helpers/cache.dart';
import 'package:my_achivement_list/models/color_group_model.dart';
import 'package:my_achivement_list/router/index.dart';

part 'color_group_settings_state.dart';

class ColorGroupSettingsCubit extends Cubit<ColorGroupSettingsState> {
  ColorGroupSettingsCubit() : super(ColorGroupSettingsInitial([]));

  final List<ColorGroupModel> _colorGroups = [];

  fetch() async {
    String cachedData = Cache.colorGroups;
    if (cachedData.isEmpty) return emit(ColorGroupSettingsInitial([]));

    final List<dynamic> cachedTasks = jsonDecode(cachedData);
    _colorGroups.addAll(cachedTasks.map((e) => ColorGroupModel.fromJson(e)));

    emit(ColorGroupSettingsInitial(_colorGroups));
  }

  Future<int> addColorGroup() async {
    final colorGroup = await RouterCore.goTo(R.ColorGroupCreateRoute);
    if (colorGroup != null && colorGroup is ColorGroupModel) {
      _colorGroups.add(colorGroup);
      Cache.colorGroups = jsonEncode(_colorGroups);
      emit(ColorGroupSettingsInitial(_colorGroups));
      return _colorGroups.length - 1;
    }
    return -1;
  }

  void removeColorGroup(int index) {
    _colorGroups.removeAt(index);
    Cache.colorGroups = jsonEncode(_colorGroups);
    emit(ColorGroupSettingsInitial(_colorGroups));
  }
}

typedef ChangeCallback = void Function(int);
