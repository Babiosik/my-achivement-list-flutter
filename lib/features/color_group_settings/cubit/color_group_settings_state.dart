part of 'color_group_settings_cubit.dart';

abstract class ColorGroupSettingsState {
  List<ColorGroupModel> get list => [];
}

class ColorGroupSettingsInitial extends ColorGroupSettingsState {
  final list;

  ColorGroupSettingsInitial(this.list);
}
