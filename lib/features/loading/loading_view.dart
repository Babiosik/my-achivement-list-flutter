import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_achivement_list/widgets/app_loader.dart';

import 'cubit/loading_cubit.dart';

class LoadingView extends StatefulWidget {
  final LoadingCubit bloc;
  LoadingView._(this.bloc);

  static Widget build() {
    print("View: LoadingView -- build");
    return BlocProvider(
      create: (context) => LoadingCubit()..fetch(),
      child: BlocBuilder<LoadingCubit, LoadingState>(
        builder: (BuildContext context, LoadingState state) {
          return LoadingView._(BlocProvider.of<LoadingCubit>(context));
        },
      ),
    );
  }

  static MaterialPageRoute route() {
    return MaterialPageRoute(builder: (context) => LoadingView.build());
  }

  @override
  _LoadingViewState createState() => _LoadingViewState();
}

class _LoadingViewState extends State<LoadingView> {
  @override
  Widget build(BuildContext context) {
    return Center(child: AppLoader());
  }
}