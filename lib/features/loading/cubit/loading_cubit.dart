import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:my_achivement_list/app/app_ex.dart';
import 'package:my_achivement_list/helpers/cache.dart';
import 'package:my_achivement_list/router/index.dart';

part 'loading_state.dart';

class LoadingCubit extends Cubit<LoadingState> {
  LoadingCubit() : super(LoadingInitial());

  fetch() async {
    await Future.wait([
      RouterCore.init(defaultRoute: R.LoadingRoute),
      Cache.init()..then((value) => AppEx.bloc.setBrightness(Cache.darkMode ? ThemeMode.dark : ThemeMode.light)),
    ]);
    RouterCore.goTo(R.MainRoute, replacement: true);
  }
}
