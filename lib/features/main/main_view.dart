import 'package:flutter/material.dart';
import 'package:my_achivement_list/features/history/history_view.dart';

class MainView extends StatefulWidget {
  MainView._();

  static Widget build() {
    print("View: MainView -- build");
    return MainView._();
  }

  static MaterialPageRoute route() {
    return MaterialPageRoute(builder: (context) => MainView.build());
  }

  @override
  _MainViewState createState() => _MainViewState();
}

class _MainViewState extends State<MainView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: HistoryView.build(),
    );
  }
}
