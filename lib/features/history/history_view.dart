import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_achivement_list/app/app_ex.dart';
import 'package:my_achivement_list/features/task/task_list/task_list_widget.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';
import 'package:my_achivement_list/router/index.dart';
import 'package:my_achivement_list/widgets/app_elevated_button.dart';

import 'cubit/history_cubit.dart';

class HistoryView extends StatefulWidget {
  final HistoryCubit bloc;
  HistoryView._(this.bloc);

  static Widget build() {
    print("View: HistoryView -- build");
    return BlocProvider(
      create: (context) => HistoryCubit()..fetch(),
      child: BlocBuilder<HistoryCubit, HistoryState>(
        builder: (BuildContext context, HistoryState state) {
          return HistoryView._(BlocProvider.of<HistoryCubit>(context));
        },
      ),
    );
  }

  static MaterialPageRoute route() {
    return MaterialPageRoute(builder: (context) => HistoryView.build());
  }

  @override
  _HistoryViewState createState() => _HistoryViewState();
}

class _HistoryViewState extends State<HistoryView> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        PageView(
          onPageChanged: (i) {
            setState(() {
              _index = i;
            });
          },
          children: [
            TaskListWidget(
              title: 'Today',
              taskList: _readyState?.today,
            ),
            TaskListWidget(
              title: 'Yesterday',
              taskList: _readyState?.yesterday,
            ),
            TaskListWidget(
              title: 'Older',
              taskList: _readyState?.older,
              isOlder: true,
            ),
          ],
        ),
        Positioned(
          top: AppSpaces.sm / 2,
          left: AppSpaces.sm,
          child: _buildIconButton(
            iconData: Icons.filter_alt,
            onTap: () => RouterCore.goTo(R.SettingsRoute),
          ),
        ),
        Positioned(
          top: AppSpaces.sm / 2,
          right: AppSpaces.sm,
          child: _buildIconButton(
            iconData: Icons.settings_outlined,
            onTap: () => RouterCore.goTo(R.SettingsRoute),
          ),
        ),
        AnimatedPositioned(
          bottom: _index == 0 ? AppSpaces.sm / 2 : -100,
          duration: Duration(milliseconds: 300),
          curve: Curves.slowMiddle,
          child: _buildCompleteButton(),
        ),
      ],
    );
  }

  SafeArea _buildIconButton({
    required IconData iconData,
    required VoidCallback? onTap,
  }) {
    return SafeArea(
      child: IconButton(
        icon: Icon(iconData, color: AppEx.theme.accentIconTheme.color),
        onPressed: onTap,
      ),
    );
  }

  AppElevatedButton _buildCompleteButton() {
    return AppElevatedButton(
      onPressed: widget.bloc.addTask,
      child: Padding(
        padding: EdgeInsets.all(AppSpaces.sm),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text('I complete'.toUpperCase()),
            AppSpaces.space(AppSpaces.sm),
            Icon(Icons.add_task_outlined),
          ],
        ),
      ),
    );
  }

  HistoryReady? get _readyState =>
      widget.bloc.state is HistoryReady ? (widget.bloc.state as HistoryReady) : null;
}
