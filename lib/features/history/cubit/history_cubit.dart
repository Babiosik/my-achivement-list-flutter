import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:my_achivement_list/helpers/cache.dart';
import 'package:my_achivement_list/models/task_model.dart';
import 'package:my_achivement_list/router/index.dart';

part 'history_state.dart';

class HistoryCubit extends Cubit<HistoryState> {
  HistoryCubit() : super(HistoryInitial());

  final List<TaskModel> todayTasks = [];
  final List<TaskModel> yesterdayTasks = [];
  final List<TaskModel> olderTasks = [];

  fetch() {
    String cachedData = Cache.taskHistory;
    if (cachedData.isEmpty) return emit(HistoryReady());

    final List<dynamic> cachedTasks = jsonDecode(cachedData);
    List<TaskModel> tempList = cachedTasks.map((e) => TaskModel.fromJson(e)).toList()
      ..sort((a, b) => a.dateTime.compareTo(b.dateTime));

    DateTime todayDate = DateTime.now();
    DateTime yesterdayDate = todayDate.add(Duration(days: -1));

    todayTasks.clear();
    yesterdayTasks.clear();
    olderTasks.clear();

    tempList.forEach((element) {
      if (_atSameDay(element.dateTime, todayDate)) {
        todayTasks.add(element);
      } else if (_atSameDay(element.dateTime, yesterdayDate)) {
        yesterdayTasks.add(element);
      } else {
        olderTasks.add(element);
      }
    });

    emit(HistoryReady(
      today: todayTasks,
      yesterday: yesterdayTasks,
      older: olderTasks,
    ));
  }

  Future addTask() async {
    final task = await RouterCore.goTo(R.TaskCreateRoute);
    if (task != null && task is TaskModel) {
      todayTasks.add(task);
      Cache.taskHistory = jsonEncode(
        [...todayTasks, ...yesterdayTasks, ...olderTasks]..sort((a, b) => a.dateTime.compareTo(b.dateTime)),
      );
      emit(HistoryReady(
        today: todayTasks,
        yesterday: yesterdayTasks,
        older: olderTasks,
      ));
    }
  }

  bool _atSameDay(DateTime element, DateTime day) {
    return element.day == day.day && element.month == day.month && element.year == day.year;
  }
}
