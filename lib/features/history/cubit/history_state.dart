part of 'history_cubit.dart';

@immutable
abstract class HistoryState {}

class HistoryInitial extends HistoryState {}

class HistoryReady extends HistoryState {
  final List<TaskModel> today;
  final List<TaskModel> yesterday;
  final List<TaskModel> older;

  HistoryReady({
    this.today = const [],
    this.yesterday = const [],
    this.older = const [],
  });
}
