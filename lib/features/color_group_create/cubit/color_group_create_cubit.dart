import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:my_achivement_list/helpers/cache.dart';
import 'package:my_achivement_list/models/color_group_model.dart';

part 'color_group_create_state.dart';

class ColorGroupCreateCubit extends Cubit<ColorGroupCreateState> {
  ColorGroupCreateCubit() : super(ColorGroupCreateInitial());

  final List<ColorGroupModel> _colors = [];

  bool _canContinueColor = false;
  bool _canContinueName = false;
  bool get canContinue => _canContinueColor && _canContinueName;

  Color _pickedColor = Colors.white;
  Color get pickedColor => _pickedColor;

  fetch() async {
    String colorsJson = Cache.colorGroups;
    if (colorsJson.isEmpty) return;
    List<dynamic> colors = jsonDecode(colorsJson);
    _colors.addAll(colors.map((e) => ColorGroupModel.fromJson(e)));
  }

  void changeColor(Color color) {
    final valid = _colors.indexWhere((element) => element.colorHex == color.value) == -1;
    _pickedColor = color;
    if (valid ^ _canContinueColor) {
      _canContinueColor = valid;
      emit(ColorGroupCreateInitial());
    }
  }

  void changeName(String name) {
    if (name.isNotEmpty ^ _canContinueName) {
      _canContinueName = name.isNotEmpty;
      emit(ColorGroupCreateInitial());
    }
  }
}
