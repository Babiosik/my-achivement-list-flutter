import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';
import 'package:my_achivement_list/models/color_group_model.dart';
import 'package:my_achivement_list/router/index.dart';
import 'package:my_achivement_list/widgets/app_create_screen.dart';

import 'cubit/color_group_create_cubit.dart';

class ColorGroupCreateView extends StatefulWidget {
  final ColorGroupCreateCubit bloc;
  ColorGroupCreateView._(this.bloc);

  static Widget build() {
    print("View: ColorGroupCreateView -- build");
    return BlocProvider(
      create: (context) => ColorGroupCreateCubit()..fetch(),
      child: BlocBuilder<ColorGroupCreateCubit, ColorGroupCreateState>(
        builder: (BuildContext context, ColorGroupCreateState state) {
          return ColorGroupCreateView._(BlocProvider.of<ColorGroupCreateCubit>(context));
        },
      ),
    );
  }

  static route() => RouterCore.route(ColorGroupCreateView.build());

  @override
  _ColorGroupCreateViewState createState() => _ColorGroupCreateViewState();
}

class _ColorGroupCreateViewState extends State<ColorGroupCreateView> {
  final _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return AppCreateScreen(
      title: 'Create color group',
      child: Column(
        children: [
          TextFormField(
            controller: _nameController,
            maxLines: 1,
            onChanged: widget.bloc.changeName,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(AppSpaces.def),
              border: InputBorder.none,
              hintText: 'Name',
            ),
          ),
          ColorPicker(
            pickerColor: widget.bloc.pickedColor,
            onColorChanged: widget.bloc.changeColor,
          ),
        ],
      ),
      onTapOk: widget.bloc.canContinue
          ? () => RouterCore.pop(
                data: ColorGroupModel(
                  colorHex: widget.bloc.pickedColor.value,
                  name: _nameController.text,
                ),
              )
          : null,
    );
  }
}
