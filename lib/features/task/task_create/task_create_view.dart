import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';
import 'package:my_achivement_list/helpers/cache.dart';
import 'package:my_achivement_list/models/color_group_model.dart';
import 'package:my_achivement_list/models/task_model.dart';
import 'package:my_achivement_list/router/router_core.dart';
import 'package:my_achivement_list/widgets/app_create_screen.dart';
import 'package:my_achivement_list/widgets/item/color_circle.dart';

class TaskCreateView extends StatefulWidget {
  TaskCreateView._();

  static Widget build() {
    print("View: TaskCreateView -- build");
    return TaskCreateView._();
  }

  static route() => RouterCore.route(TaskCreateView.build());

  @override
  _TaskCreateViewState createState() => _TaskCreateViewState();
}

class _TaskCreateViewState extends State<TaskCreateView> {
  final _messageController = TextEditingController();
  final List<ColorGroupModel> _colorGroups = [];
  ColorGroupModel _selected = ColorGroupModel.zero;
  bool _canContinue = false;

  @override
  void initState() {
    String colorsJson = Cache.colorGroups;
    if (colorsJson.isEmpty) return;
    List<dynamic> colors = jsonDecode(colorsJson);
    _colorGroups.addAll(colors.map((e) => ColorGroupModel.fromJson(e)));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AppCreateScreen(
      title: 'Create task',
      child: Column(
        children: [
          DropdownButton<ColorGroupModel>(
            value: _selected,
            onChanged: (color) => setState(() => _selected = color ?? ColorGroupModel.zero),
            isExpanded: true,
            isDense: false,
            underline: Container(),
            items: List.generate(_colorGroups.length + 1, _buildColorGroupItem),
          ),
          Divider(),
          TextFormField(
            controller: _messageController,
            maxLines: null,
            onChanged: (value) {
              if ((_canContinue && value.isEmpty) || (!_canContinue && value.isNotEmpty)) {
                setState(() => _canContinue = value.isNotEmpty);
              }
            },
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(AppSpaces.def),
              border: InputBorder.none,
            ),
          ),
        ],
      ),
      onTapOk: _canContinue
          ? () => RouterCore.pop(
                data: TaskModel(
                  dateTime: DateTime.now(),
                  text: _messageController.text,
                  colorGroup: _selected,
                ),
              )
          : null,
    );
  }

  DropdownMenuItem<ColorGroupModel> _buildColorGroupItem(index) {
    ColorGroupModel model = index == 0 ? ColorGroupModel.zero : _colorGroups[index - 1];
    return DropdownMenuItem(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: AppSpaces.sm),
        child: Row(
          children: [
            ColorCircle(model.color),
            AppSpaces.spaceW(AppSpaces.sm),
            Text(model.name),
          ],
        ),
      ),
      value: model,
    );
  }
}
