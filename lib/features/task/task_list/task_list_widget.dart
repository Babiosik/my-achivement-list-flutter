import 'package:flutter/material.dart';
import 'package:my_achivement_list/features/task/task_list/task_item_widget.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';
import 'package:my_achivement_list/models/task_model.dart';
import 'package:my_achivement_list/widgets/app_loader.dart';

class TaskListWidget extends StatelessWidget {
  const TaskListWidget({
    Key? key,
    required this.title,
    required this.taskList,
    this.isOlder = false,
  }) : super(key: key);

  final String title;
  final List<TaskModel>? taskList;
  final bool isOlder;

  @override
  Widget build(BuildContext context) {
    Widget child;
    if (taskList == null) {
      child = AppLoader();
    } else if (taskList!.length == 0) {
      child = Center(child: Text('no data'));
    } else {
      child = _buildFill(taskList!);
    }
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(title),
      ),
      body: child,
    );
  }

  Widget _buildFill(List<TaskModel> list) {
    return ListView.builder(
      padding: EdgeInsets.symmetric(vertical: AppSpaces.def),
      itemCount: list.length + 1,
      itemBuilder: (ctx, index) {
        int i = list.length - index - 1;
        if (i < 0) return Divider();

        return TaskListItemWidget(task: list[i], isNeedDate: isOlder);
      },
    );
  }
}
