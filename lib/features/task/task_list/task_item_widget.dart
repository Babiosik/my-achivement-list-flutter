import 'package:flutter/material.dart';
import 'package:my_achivement_list/helpers/app_formates.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';
import 'package:my_achivement_list/models/color_group_model.dart';
import 'package:my_achivement_list/models/task_model.dart';
import 'package:my_achivement_list/router/index.dart';
import 'package:my_achivement_list/widgets/app_ripple.dart';
import 'package:my_achivement_list/widgets/item/color_circle.dart';

class TaskListItemWidget extends StatelessWidget {
  const TaskListItemWidget({
    Key? key,
    required this.task,
    this.isNeedDate = false,
  }) : super(key: key);

  final TaskModel task;
  final bool isNeedDate;

  @override
  Widget build(BuildContext context) {
    return AppRipple(
      onTap: () => RouterCore.goTo(R.TaskRoute, arguments: task),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: AppSpaces.sm, horizontal: AppSpaces.def),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                if (task.colorGroup != ColorGroupModel.zero) ...[
                  ColorCircle(task.colorGroup.color),
                  AppSpaces.space(AppSpaces.sm),
                ],
                Expanded(child: Divider()),
                AppSpaces.space(AppSpaces.sm),
                Text(AppFormates.dateTimeToTime(task.dateTime)),
                if (isNeedDate) ...[
                  AppSpaces.space(AppSpaces.sm),
                  Text(AppFormates.dateTimeToDate(task.dateTime)),
                ],
              ],
            ),
            Flexible(
              child: Padding(
                padding: EdgeInsets.only(left: AppSpaces.lg),
                child: Text(
                  task.text,
                  softWrap: true,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                  textAlign: TextAlign.justify,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
