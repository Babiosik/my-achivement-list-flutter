import 'package:flutter/material.dart';
import 'package:my_achivement_list/models/task_model.dart';
import 'package:my_achivement_list/widgets/dismissible_screen.dart';
import 'package:my_achivement_list/widgets/unify/app_error.dart';

class TaskView extends StatefulWidget {
  const TaskView._(this.task);

  final TaskModel task;

  static Widget build(TaskModel task) {
    print("View: TaskView -- build");
    return TaskView._(task);
  }

  static route(Object? arguments) => DismissibleScreen.route(() {
        if (arguments != null && arguments is TaskModel) {
          return TaskView.build(arguments);
        } else {
          return AppError(additionalMessage: arguments.runtimeType.toString());
        }
      });

  @override
  _TaskViewState createState() => _TaskViewState();
}

class _TaskViewState extends State<TaskView> {
  @override
  Widget build(BuildContext context) {
    return DismissibleScreen(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Task #${widget.task.hashCode}'),
          backgroundColor: widget.task.colorGroup.color,
        ),
      ),
    );
  }
}
