import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';
import 'package:my_achivement_list/router/index.dart';
import 'package:my_achivement_list/widgets/app_back_button.dart';
import 'package:my_achivement_list/widgets/dismissible_screen.dart';
import 'package:my_achivement_list/widgets/switches/app_switch_custom.dart';

import 'cubit/settings_cubit.dart';

class SettingsView extends StatefulWidget {
  final SettingsCubit bloc;
  SettingsView._(this.bloc);

  static Widget build() {
    print("View: SettingsView -- build");
    return BlocProvider(
      create: (context) => SettingsCubit(),
      child: BlocBuilder<SettingsCubit, SettingsState>(
        builder: (BuildContext context, SettingsState state) {
          return SettingsView._(BlocProvider.of<SettingsCubit>(context));
        },
      ),
    );
  }

  static PageRouteBuilder route() => DismissibleScreen.route(() => SettingsView.build());

  @override
  _SettingsViewState createState() => _SettingsViewState();
}

class _SettingsViewState extends State<SettingsView> {
  @override
  Widget build(BuildContext context) {
    return DismissibleScreen(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Settings'),
          leading: AppBackButton(),
        ),
        body: ListView(
          children: [
            AppSwitchCustom(
              checked: true,
              child: Text('Dark mode'),
              iconOn: Icon(Icons.brightness_4_outlined),
              iconOff: Icon(Icons.brightness_3_outlined),
              onToggle: (_) => widget.bloc.toggleTheme(),
              ripple: true,
              padding: EdgeInsets.symmetric(vertical: AppSpaces.sm, horizontal: AppSpaces.def),
            ),
            Divider(),
            _button(
              'Color groups',
              () => RouterCore.goTo(R.ColorGroupSettingsRoute),
            ),
            Divider(),
          ],
        ),
      ),
    );
  }

  Widget _button(String text, VoidCallback onTap) {
    return InkWell(
      onTap: onTap,
      child: Padding(
        padding: EdgeInsets.all(AppSpaces.def),
        child: Row(
          children: [
            Flexible(
              child: Text(text),
            ),
          ],
        ),
      ),
    );
  }
}
