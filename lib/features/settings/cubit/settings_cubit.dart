import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:my_achivement_list/app/app_ex.dart';

part 'settings_state.dart';

class SettingsCubit extends Cubit<SettingsState> {
  SettingsCubit() : super(SettingsInitial());

  bool get darkMode => AppEx.state.themeMode == ThemeMode.dark;
  void toggleTheme() {
    AppEx.bloc.setBrightness(darkMode ? ThemeMode.light : ThemeMode.dark);
    emit(SettingsInitial());
  }
}
