import 'package:flutter/cupertino.dart';

abstract class AppSpaces {
  AppSpaces._();

  static double get xlg => 32.0;
  static double get lg => 24.0;
  static double get def => 16.0;
  static double get sm => 8.0;

  static Widget space(double size) => SizedBox(width: size, height: size);
  static Widget spaceH(double size) => SizedBox(height: size);
  static Widget spaceW(double size) => SizedBox(width: size);
}
