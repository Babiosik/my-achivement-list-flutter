import 'package:shared_preferences/shared_preferences.dart';

abstract class Cache {
  Cache._();

  static late SharedPreferences _sp;

  static Future init() async {
    _sp = await SharedPreferences.getInstance();
  }

  static String get taskHistory => _sp.getString('taskHistory') ?? '';
  static set taskHistory(String value) => _sp.setString('taskHistory', value);

  static String get colorGroups => _sp.getString('colorGroups') ?? '';
  static set colorGroups(String value) => _sp.setString('colorGroups', value);

  static bool get darkMode => _sp.getBool('darkMode') ?? false;
  static set darkMode(bool value) => _sp.setBool('darkMode', value);
}
