import 'package:intl/intl.dart';

class AppFormates {
  AppFormates._();

  static String dateTimeToTime(DateTime dateTime) {
    return DateFormat(DateFormat.HOUR24_MINUTE_SECOND).format(dateTime);
  }

  static String dateTimeToDate(DateTime dateTime) {
    return DateFormat("dd.MM.yyyy").format(dateTime);
  }
}
