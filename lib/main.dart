import 'package:flutter/material.dart';
import 'package:my_achivement_list/app/app.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(App.build());
}
