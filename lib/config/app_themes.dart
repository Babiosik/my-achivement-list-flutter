import 'package:flutter/material.dart';

class AppThemes {
  AppThemes._();

  static ThemeData get light => ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.blue,
        dividerTheme: DividerThemeData(
          color: Colors.black45,
          thickness: 1,
          space: 0,
        ),
        accentIconTheme: IconThemeData(color: Colors.white),
      );

  static ThemeData get dark => ThemeData(
        brightness: Brightness.dark,
        dividerTheme: DividerThemeData(
          color: Colors.white30,
          thickness: 2,
          space: 0,
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.resolveWith((states) {
              return Colors.black87;
            }),
          ),
        ),
        accentIconTheme: IconThemeData(color: Colors.white),
      );
}
