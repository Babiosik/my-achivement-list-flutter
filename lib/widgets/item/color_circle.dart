import 'package:flutter/material.dart';

class ColorCircle extends StatelessWidget {
  const ColorCircle(
    this.color, {
    Key? key,
    this.size,
  }) : super(key: key);

  final Color color;
  final Size? size;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size?.width ?? 16,
      height: size?.height ?? 16,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: color,
      ),
    );
  }
}
