import 'package:flutter/material.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';

class AppError extends StatelessWidget {
  const AppError({
    Key? key,
    this.message,
    this.additionalMessage,
  }) : super(key: key);

  final String? message;
  final String? additionalMessage;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.all(AppSpaces.def),
              child: Text(message ?? 'Something going'),
            ),
          ],
        ),
        if (additionalMessage != null)
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(AppSpaces.def),
                child: Text(additionalMessage!),
              ),
            ],
          )
      ],
    );
  }
}
