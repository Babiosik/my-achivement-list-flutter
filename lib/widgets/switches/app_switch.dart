import 'package:flutter/material.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';

class AppSwitch extends StatefulWidget {
  const AppSwitch({
    Key? key,
    required this.checked,
    required this.child,
    this.onToggle,
    this.padding,
  }) : super(key: key);

  final bool checked;
  final Widget child;
  final void Function(bool)? onToggle;
  final EdgeInsets? padding;

  @override
  _AppSwitchState createState() => _AppSwitchState();
}

class _AppSwitchState extends State<AppSwitch> {
  bool _checked = false;

  @override
  void initState() {
    _checked = widget.checked;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () {
        setState(() => _checked = !_checked);
        widget.onToggle?.call(_checked);
      },
      child: Padding(
        padding: widget.padding ?? const EdgeInsets.symmetric(horizontal: 8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(child: widget.child),
            AppSpaces.spaceW(AppSpaces.sm),
            Switch(
              value: _checked,
              onChanged: (value) {
                setState(() => _checked = value);
                widget.onToggle?.call(value);
              },
            ),
          ],
        ),
      ),
    );
  }
}
