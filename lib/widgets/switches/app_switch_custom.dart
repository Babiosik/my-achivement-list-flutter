import 'package:flutter/material.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';

class AppSwitchCustom extends StatefulWidget {
  const AppSwitchCustom({
    Key? key,
    required this.checked,
    required this.child,
    required this.iconOn,
    required this.iconOff,
    this.onToggle,
    this.padding,
    this.ripple = false,
  }) : super(key: key);

  final bool checked;
  final Widget child;
  final Widget iconOn;
  final Widget iconOff;
  final void Function(bool)? onToggle;
  final EdgeInsets? padding;
  final bool ripple;

  @override
  _AppSwitchCustomState createState() => _AppSwitchCustomState();
}

class _AppSwitchCustomState extends State<AppSwitchCustom> {
  bool _checked = false;

  @override
  void initState() {
    _checked = widget.checked;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Widget child = Padding(
      padding: widget.padding ?? const EdgeInsets.symmetric(horizontal: 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(child: widget.child),
          AppSpaces.spaceW(AppSpaces.sm),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: _checked ? widget.iconOn : widget.iconOff,
          ),
        ],
      ),
    );
    if (widget.ripple) {
      return InkWell(
        onTap: () {
          setState(() => _checked = !_checked);
          widget.onToggle?.call(_checked);
        },
        child: child,
      );
    } else {
      return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          setState(() => _checked = !_checked);
          widget.onToggle?.call(_checked);
        },
        child: child,
      );
    }
  }
}
