import 'package:flutter/material.dart';
import 'package:my_achivement_list/app/app_ex.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';
import 'package:my_achivement_list/router/index.dart';

class AppCreateScreen extends StatelessWidget {
  const AppCreateScreen({
    Key? key,
    required this.title,
    required this.child,
    this.onTapOk,
  }) : super(key: key);

  final String title;
  final Widget child;
  final VoidCallback? onTapOk;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.5),
      body: Padding(
        padding: EdgeInsets.all(AppSpaces.lg * 2),
        child: Center(
          child: Container(
            width: double.infinity,
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
              color: AppEx.theme.canvasColor,
              borderRadius: BorderRadius.circular(AppSpaces.def),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: EdgeInsets.all(AppSpaces.def),
                  child: Text(title),
                ),
                Divider(height: 1),
                Flexible(
                  child: SingleChildScrollView(child: child),
                ),
                Divider(height: 1),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Expanded(
                      child: TextButton(
                        onPressed: () => RouterCore.pop(),
                        child: Text('Cancel'),
                        style: ButtonStyle(
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        ),
                      ),
                    ),
                    Expanded(
                      child: TextButton(
                        onPressed: onTapOk,
                        child: Text('Ok'),
                        style: ButtonStyle(tapTargetSize: MaterialTapTargetSize.shrinkWrap),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
