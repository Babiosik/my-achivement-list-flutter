import 'package:flutter/material.dart';
import 'package:my_achivement_list/router/index.dart';

class AppBackButton extends StatelessWidget {
  const AppBackButton({
    Key? key,
    this.onTap,
  }) : super(key: key);

  final VoidCallback? onTap;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: onTap ?? () => RouterCore.pop(),
      icon: Icon(Icons.keyboard_arrow_left_outlined),
    );
  }
}
