import 'package:flutter/material.dart';
import 'package:my_achivement_list/helpers/app_spaces.dart';

class AppIconText extends StatelessWidget {
  const AppIconText({
    Key? key,
    required this.icon,
    this.text,
    this.child,
    this.space,
  })  : assert(text != null || child != null),
        super(key: key);

  final Widget icon;
  final String? text;
  final Widget? child;
  final double? space;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        icon,
        AppSpaces.spaceW(space ?? AppSpaces.sm),
        text != null ? Text(text!) : child!,
      ],
    );
  }
}
